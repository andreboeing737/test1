# Test Application

This is test application created by Andreas Alexander for test purpose. The application consist of consist of Android app (Using React Native), Backend app (Node JS), and Data-getter thirdy party service (Node JS). The application from Android app will get data from Backend using API secured with authorization header which encrypted with Aes256. The backend will get the data from thirdy party service which also encrypted with Aes256 in both Authorization and Body.

## Installation
First of all, make sure you have Node JS, Android Studio, and MongoDB, and expo installed.

To install expo, install it using
```bash
npm install expo-cli -g
```

1. Clone the project into your local folder using Git clone.

2. Once cloned, open the Backend folder, then do install package using
```bash
npm install
```
3. When package installation finished, run the backend using
```bash
nodemon
```

4. Do the same above step into data_getter folder

5. Open app/my-app/ folder and install package using
```bash
npm install
```
6. Once finished, run
```bash
expo start
```
7. The metro bundler will be opened and app ready to use.
8. Press 'a' to run the android simulator

## Usage
The home screen will be like this

![Home Screen](https://gcdn.pbrd.co/images/TY0keozQ6UJY.png?o=1)

The application has some feature :

1. Search by name

Input any number in "Search by Name" and click search, then specified data will be showed
![search](https://gcdn.pbrd.co/images/nQyVePqugkoK.png?o=1)

2. Search by Category

Select Category for "even" or "odd" , once changed, the data will be refreshed
![filter](https://gcdn.pbrd.co/images/1jKkS9bw60jf.png?o=1)

3. Bookmark row

Click on the Bookmark icon to bookmark/unbookmark item. The gold star icon indicating the item being bookmarked in the table view, or the red button will be indicating item being bookmarked in detail view.
![bookmark](https://gcdn.pbrd.co/images/F09cMAA67UnC.png?o=1)


4. Go into detail page

Click the image or the name, then app will be redirected into detail page
![detail](https://gcdn.pbrd.co/images/F09cMAA67UnC.png?o=1)
