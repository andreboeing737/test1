const credentials = require('./credentials.js');

const config = {
	"PORT":8081,
	"ENCRYPTION_KEY":credentials.ENCRYPTION_KEY,
	"ENCRYPTION_KEY_HEADER":credentials.ENCRYPTION_KEY_HEADER,
	"API_URL":"https://app.mhc.asia/test/photos.json"
}

module.exports = config;