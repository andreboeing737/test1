const config = require('./config/config');
const express = require('express');
const app = express();
const {validateHeader,validateData} = require('./middlewares/validator');
const {encryptHeaderKey,decrypt} = require('./middlewares/encryption');
const {requestData} = require('./middlewares/core')
var bodyParser = require('body-parser')


app.use( bodyParser.json() );       
app.use(bodyParser.urlencoded({     
  extended: true
})); 

app.get('/', (req, res) => {
  var headers = req.headers;
  var params = req.query;
  if (validateHeader(headers)){
    requestData(params,function(err,data){
      if (!err){
        res.status(200).send(data);
      } else {
        res.status(err.status).send(err);
      }
    });
  } else {
    res.status(403).send({'code':'403','message':'Authentication failed'});
  }
})

app.listen(config.PORT, () => {
  console.log(`Listening at port :${config.PORT}`)
})