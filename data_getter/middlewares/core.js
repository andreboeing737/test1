const config = require('../config/config');
const https = require('https');
const axios = require('axios');

var options = {
    host: config.API_URL,
    path: config.API_URL_PATH,
    method: 'GET',
    headers: {
        accept: 'application/json'
    }
};

var requestData = function(params,callback){
    axios.get(config.API_URL)
    .then(function (response) {
        var data = response.data;
        var result = parseData(params,data);
        callback(null,result);
    })
    .catch(function (error) {
        console.log(error);
        callback(data,null);
    })
}

function parseData(params,datas){
    var result = [];
    if (Object.keys(params).length){
        if (typeof params.id != 'undefined'){
            datas.map((data)=>{
                if (data.id == params.id){
                    result.push(data);
                }
            });
        } else if (typeof params.filter != 'undefined'){
            datas.map((data)=>{
                if (data.category_name == params.filter){
                    result.push(data);
                }
            });
        }
    } else {
        result = datas;
    }
    return result;
}

exports.requestData = requestData;