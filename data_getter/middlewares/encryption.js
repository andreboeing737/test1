'use strict';
const config = require('../config/config')
const CryptoJS = require("crypto-js");

const ENC_KEY = config.ENCRYPTION_KEY;
const IV = config.ENCRYPTION_IV;
const ENC_KEY_HEADER = config.ENCRYPTION_KEY_HEADER;
const IV_HEADER = config.ENCRYPTION_IV_HEADER;

var encrypt = ((val) => {
  const encrypted = CryptoJS.AES.encrypt(val, ENC_KEY).toString();;
  return encrypted;
});

var decrypt = ((encrypted) => {
  const decrypted = CryptoJS.AES.decrypt(encrypted, ENC_KEY).toString(CryptoJS.enc.Utf8);
  return decrypted;
});

var encryptHeaderKey = ((val) => {
  const encrypted = CryptoJS.AES.encrypt(val, ENC_KEY_HEADER).toString();;
  return encrypted;
});

var decryptHeaderKey = ((encrypted) => {
  const decrypted = CryptoJS.AES.decrypt(encrypted, ENC_KEY_HEADER).toString(CryptoJS.enc.Utf8);;
  return decrypted;
});

exports.encrypt = encrypt;
exports.decrypt = decrypt;
exports.encryptHeaderKey = encryptHeaderKey;
exports.decryptHeaderKey = decryptHeaderKey;