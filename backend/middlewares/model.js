const { MongoClient } = require("mongodb");
var config , {constants} = require('../config/config');

const uri = constants.MONGODB_HOST+":"+constants.MONGODB_PORT+"?maxPoolSize=20&w=majority";

function initConnection(callback){
    MongoClient.connect(uri,function(err,client){
      if (!err){
        var client = client.db(constants.MONGODB_DATABASE);
        console.log("Connected successfully to mongodb server");
        callback(null,client);
      } else {
        console.log(e);
        callback(e,null);
      }
    });
}

function insertUpdate(query,update,callback){
  initConnection(function(err,db){
    if (!err){
      db.collection(constants.TABLE_BOOKMARK).updateOne(query,update,{upsert:true},function(err,res){
        if (!err){
          callback(null,res);
        } else {
          callback(err,null);
        }
      });
    }
  })
}

function getAll(callback){
  initConnection(function(err,db){
    if (!err){
      db.collection(constants.TABLE_BOOKMARK).find({}).toArray(function(err,cursor){
        console.log(cursor);
        if (!err){
          callback(null,cursor);
        } else {
          callback(err,null);
        }
      });
    }
  })
}

exports.insertUpdate = insertUpdate
exports.getAll = getAll