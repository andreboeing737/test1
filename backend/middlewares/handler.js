const {config} = require('../config/config');
const axios = require('axios');
const {encrypt, encryptHeaderKey} = require('./encryption');
const credentials = require('../config/credentials');
const model = require('./model');

var doGetData = function(params,callback){
    var headers = {
        authorization:getApiAuthorization()
    }
    var url = config.API_URL.DATA_GETTER;
    if (Object.keys(params).length){
        var query_url = new URLSearchParams(params);
        var query_string = query_url.toString();
        url += "?"+query_string;
    }
    axios.get(url, {headers:headers})
    .then(function (response) {
        console.log(response.data);
        callback(null,response.data);
    })
    .catch(function (error) {
        console.log(error);
        callback(error,null);
    })
};

var updateBookmark = function(data,callback){
    var query = {
        '_id':data.id
    }
    var update = [
        {
            $set: {'bookmarked':data.bookmarked}
        }
    ]
    model.insertUpdate(query,update,function(err,res){
        if (!err){
            var result = {"status":200,"message":"Update successful"};
            callback(null,result);
        } else {
            callback(err,null);
        }
    });
}

var getBookmark = function(callback){
    model.getAll(function(err,res){
        if (!err){
            var data = JSON.stringify(res);
            data = {data:encrypt(data)};
            callback(null,data);
        } else {
            callback(err,null);
        }
    });
}

function getApiAuthorization(){
    return encryptHeaderKey(credentials.HEADER_KEY);
}

exports.doGetData = doGetData;
exports.updateBookmark = updateBookmark;
exports.getBookmark = getBookmark