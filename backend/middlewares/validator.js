const {encrypt,decrypt,decryptHeaderKey} = require('./encryption');
const credentials = require('../config/credentials');

var validateHeader = function(headers){
	if (typeof headers.authorization != 'undefined'){
		try{
			var decrypted = decryptHeaderKey(headers.authorization);
			if (decrypted == credentials.HEADER_KEY){
				return true
			} else {
				return false
			}
		} catch(e) {
			console.log(e);
			console.log("Decryption failed");
			return false;
		}
	} else {
		console.log("Authorization not exist in headers");
		return false
	}
}

var validateData = function(body){
	if (typeof body != 'undefined' && typeof body.data != 'undefined'){
		try{
			if (decrypt(body.data)){
				return true
			} else {
				return false
			}
		} catch(e) {
			console.log(e);
			console.log("Decryption failed");
			return false;
		}
	} else {
		console.log("body or body.data not defined");
		return false
	}
}

exports.validateHeader = validateHeader
exports.validateData = validateData