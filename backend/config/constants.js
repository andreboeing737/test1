const constants = {
	"MONGODB_HOST":"mongodb://127.0.0.1",
	"MONGODB_PORT":"27017",
	"MONGODB_DATABASE":"test",
	"TABLE_BOOKMARK":"bookmark"
}

module.exports = constants