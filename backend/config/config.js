const credentials = require('./credentials.js');
const constants = require('./constants');

const config = {
	"PORT":8080,
	"ENCRYPTION_KEY":credentials.ENCRYPTION_KEY,
	"ENCRYPTION_KEY_HEADER":credentials.ENCRYPTION_KEY_HEADER,
	"API_URL":{
		"DATA_GETTER":"http://localhost:8081"
	}
}

exports.config = config;
exports.constants = constants;