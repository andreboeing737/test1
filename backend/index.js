const {config} = require('./config/config');;
const express = require('express');
const app = express();
const {validateHeader,validateData} = require('./middlewares/validator');
const {encryptHeaderKey,decrypt} = require('./middlewares/encryption');
const {doGetData,updateBookmark,getBookmark} = require('./middlewares/handler');
const bodyParser = require('body-parser')

app.use( bodyParser.json() );       
app.use(bodyParser.urlencoded({     
  extended: true
})); 

app.get('/get-data', (req, res) => {
  var headers = req.headers;
  var params = req.query;
  if (validateHeader(headers)){
    doGetData(params,function(err,data){
      if (!err){
        console.log(data);
        res.status(200).send(data);
      } else {
        res.status(err.response.status).send(err.response.data.message);
      }
    })
  } else {
    res.status(403).send({'code':'403','message':'Authentication failed'});
  }
})

app.post('/update-bookmark', (req, res) => {
  var headers = req.headers;
  var body = req.body;
  if (validateHeader(headers)){
    if (validateData(body)){
      var data = body.data;
      console.log(data);
      data = decrypt(data);
      data = JSON.parse(data);
      updateBookmark(data,function(err,data){
        if (!err){
          res.status(200).send(data);
        } else {
          res.status(err).send(err);
        }
      })
    } else {
      res.status(403).send({'code':'403','message':'Body encryption mismatch'});
    }
  } else {
    res.status(403).send({'code':'403','message':'Authentication failed'});
  }
})

app.get('/get-bookmark', (req, res) => {
  var headers = req.headers;
  var body = req.body;
  if (validateHeader(headers)){
    getBookmark(function(err,data){
      if (!err){
        console.log(data);
        res.status(200).send(data);
      } else {
        res.status(err).send(err);
      }
    })
  } else {
    res.status(403).send({'code':'403','message':'Authentication failed'});
  }
})

app.listen(config.PORT, () => {
  console.log(`Listening at port :${config.PORT}`)
})