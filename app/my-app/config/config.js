const credentials = require('./credentials.js');

const config = {
	"PORT":8080,
	"ENCRYPTION_KEY":credentials.ENCRYPTION_KEY,
	"ENCRYPTION_IV":credentials.ENCRYPTION_IV,
	"ENCRYPTION_KEY_HEADER":credentials.ENCRYPTION_KEY_HEADER,
	"ENCRYPTION_IV_HEADER":credentials.ENCRYPTION_IV_HEADER,
	"HEADER_KEY":credentials.HEADER_KEY,
	"API_URL":{
		"BACKEND_GET_DATA":"http://192.168.43.53:8080/get-data",
		"BACKEND_GET_BOOKMARK":"http://192.168.43.53:8080/get-bookmark",
		"BACKEND_UPDATE_BOOKMARK":"http://192.168.43.53:8080/update-bookmark"
	}
}

module.exports = config;