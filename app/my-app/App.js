import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, ScrollView, Image, Button, TouchableHighlight, TouchableOpacity, TextInput } from 'react-native';
import { Picker } from '@react-native-picker/picker';
import { Row, Table } from 'react-native-table-component';
import {doGetData,doGetBookmark,doUpdateBookmark} from './middlewares/core';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import fontawesome from '@fortawesome/fontawesome'
import { faCheckSquare, faStar, faArrowLeft } from '@fortawesome/fontawesome-free-solid'
import ImageViewer from 'react-native-image-zoom-viewer';

fontawesome.library.add(faCheckSquare, faStar, faArrowLeft);

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      table_head: ['Info', 'Image', 'Bookmark'],
      table_width: [130, 130, 130],
      table_data: [],
      bookmark: [],
      search_id_value: null,
      search_filter_value: null,
      detail_view_object:{
        id:null,
        category:null,
        url:null
      }
    }
    this.isBookmarked = this.isBookmarked.bind(this);
    this.updateBookmark = this.updateBookmark.bind(this);
    this.updateTableData = this.updateTableData.bind(this);
    this.updateBookmarkData = this.updateBookmarkData.bind(this);
    this.updateInputId = this.updateInputId.bind(this);
    this.updateInputFilter = this.updateInputFilter.bind(this);
    this.backToTableView = this.backToTableView.bind(this);
    this.goToDetailView = this.goToDetailView.bind(this);
  }

  backToTableView(){
    this.setState({detail_view_object:{id:null,category:null,url:null}});
  }

  goToDetailView(data){
    this.setState({
      detail_view_object:{
        id:data.id,
        category:data.category,
        url:data.url
      }
    });
  }

  isBookmarked(id){
    var bookmark_list = this.state.bookmark;
    for (var i = 0; i < bookmark_list.length; i++) {
      if (bookmark_list[i]._id == id && bookmark_list[i].bookmarked){
        return true;
      }
    }
    return false;
  }

  updateBookmark(id){
    var self = this;
    console.log("Update bookmark");
    var bookmarked = this.isBookmarked(id) ? false : true;
    var data = {
      'id':id,
      'bookmarked':bookmarked
    }
    doUpdateBookmark(data,function(err,res){
      console.log("do api update bookmark");
      if (!err){
        self.updateBookmarkData();
        self.updateTableData();
      }
    });
  }

  updateBookmarkData(){
    var self = this;
    console.log("get bookmark");
    var state = this.state;
    var bookmark = state.bookmark;
    doGetBookmark(function(err,res){
      if (!err){
        self.setState({bookmark:res}, () => self.updateTableData());
      }
    });
  }

  updateInputId(val){
    this.setState({search_id_value:val,search_filter_value:""});
  }

  updateInputFilter(val){
    var self = this;
    this.setState({search_filter_value:val,search_id_value:""},()=>self.updateTableData());
  }

  updateTableData(){
    var self = this;
    var state = this.state;
    var datas = [];
    var params = {};
    if (this.state.search_id_value){
      params = {id:this.state.search_id_value}
    } else if (this.state.search_filter_value){
      params = {filter:this.state.search_filter_value}
    }
    doGetData(params,function(err,res){
      if (!err){
        console.log(res);
        res.map((data)=>{
          let info = (
            <TouchableOpacity onPress={()=>self.goToDetailView(data)}>
              <View style={{justifyContent: 'center',alignItems: 'center'}}>
                <Text>Name : {data.id}</Text>
                <Text>Description : {data.category}</Text>
              </View>
            </TouchableOpacity>
          )
          let img = (
            <TouchableOpacity onPress={()=>self.goToDetailView(data)}>
              <View style={{justifyContent: 'center',alignItems: 'center'}}>
              <Image source={{uri: data.url}}
                style={{width: 40, height: 40}} />
              </View>
            </TouchableOpacity>
          )
          let bookmark_btn = (
            <View style={{justifyContent: 'center',alignItems: 'center'}}>
              <TouchableOpacity onPress={()=>self.updateBookmark(data.id)}>
                  <View>
                    <FontAwesomeIcon icon="star" color={self.isBookmarked(data.id) ? '#Dfe418' : 'black'} />
                  </View>
              </TouchableOpacity>
            </View>
          )
          let row = [info,img,bookmark_btn];
          datas.push(row);
        });
        self.setState({table_data: datas});
      }
    });
  }

  render() {
    console.log("render");
    var self = this;
    var state = this.state;

    var view_table_style = {
      'display':state.detail_view_object.id ? 'none' : 'flex'
    }

    var view_detail_style = {
      'display':state.detail_view_object.id ? 'flex' : 'none'
    }

    if (!this.state.bookmark.length){
      this.updateBookmarkData();
    }
    if (!this.state.table_data.length){
      this.updateTableData();
    }

    var is_detail_bookmarked = this.isBookmarked(state.detail_view_object.id);
    var detail_bookmark_btn_text = is_detail_bookmarked ? "Remove Bookmark" : "Add to Bookmark";
    var detail_bookmark_btn_color = is_detail_bookmarked ? "red" : "green";
    var detail_image_url = [
      {
        "url":state.detail_view_object.url?state.detail_view_object.url:"https://app.mhc.asia/test/9@2x.png",
      }
    ];
    console.log(detail_image_url);

    return (
      <View style={styles.container}>
        <View style={view_table_style}>
          <View style={{marginTop:30}}>
            <View>
              <Text style={styles.title}>Test Application</Text>
            </View>
            <View style={styles.searchSection}>
              <TextInput style={styles.input} placeholder="Search by Name" keyboardType="numeric" value={state.search_id_value} onChangeText={(text)=>self.updateInputId(text)}/>
              <View styles={styles.btn_search}>
                <Button title="Search" styles={{backgroundColor:'#2596be'}} onPress={()=>self.updateTableData()}/>
              </View>
            </View>
            <View style={{marginBottom:10}}>
              <Text>Filter by Category</Text>
            </View>
            <View style={styles.filterSection}>
              <Picker selectedValue={state.search_filter_value} style={styles.input_picker} onValueChange={(itemValue,itemIndex)=>self.updateInputFilter(itemValue)}>
                <Picker.Item label="Nothing" value=""/>
                <Picker.Item label="Even" value="even"/>
                <Picker.Item label="Odd" value="odd"/>
              </Picker>
            </View>
          </View>
          <ScrollView horizontal={true}>
            <View>
              <Table>
                <Row data={state.table_head} widths={state.table_width} style={styles.headerWrapper} textStyle={styles.text}/>
              </Table>
              <ScrollView style={styles.dataWrapper}>
                <Table>
                  {
                    state.table_data.map((data_row, index) => (
                      <Row
                        key={index}
                        data={data_row}
                        textStyle={styles.text}
                        widthArr={state.table_width}
                        style={[styles.row, index%2 && {backgroundColor: '#ffffff'}]}
                      />
                    ))
                  }
                </Table>
              </ScrollView>
            </View>
          </ScrollView>
        </View>
        <View style={view_detail_style}>
          <View style={{marginTop:0}}>
            <View style={{height:100,flexDirection:'row'}}>
              <TouchableOpacity onPress={(()=>self.backToTableView())} style={{marginRight: 20,marginTop: 6}}>
                <View>
                  <FontAwesomeIcon icon="arrow-left" color='#2596be' />
                </View>
              </TouchableOpacity>
              <Text style={styles.title}>Detail View</Text>
            </View>
          </View>
          <View style={{justifyContent: 'center',alignItems: 'center',marginBottom: 30}}>
            <View style={{width:250,height:250,backgroundColor: '#F5FCFFF'}}>
              <ImageViewer style={{backgroundColor: '#F5FCFFF'}} imageUrls={detail_image_url} renderIndicator={() => null} />
            </View>
          </View>
          <View style={styles.options_detail}>
            <View style={styles.options_child}>
              <View>
                <Text style={{fontWeight:'bold'}}>Name : {state.detail_view_object.id}</Text>
                <Text style={{fontWeight:'bold'}}>Description : {state.detail_view_object.category}</Text>
              </View>
            </View>
            <View style={styles.options_child}>
              <Button title={detail_bookmark_btn_text} color={detail_bookmark_btn_color} onPress={()=>self.updateBookmark(state.detail_view_object.id)}/>
            </View>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: { 
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    padding: '2%'
  },
  headerWrapper: { 
    height: 40,
    backgroundColor: '#2596be',
    justifyContent: 'center',
    alignItems: 'center'
  },
  text: { 
    textAlign: 'center',
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold'
  },
  row: { 
    height: 60, 
    backgroundColor: '#F6F7Fb',
    justifyContent: 'center',
    alignItems: 'center'
  },
  searchSection: {
    marginBottom: 10,
    height: 40,
    flexDirection:'row'
  },
  filterSection: {
    marginBottom: 20,
    width: 240,
    height: 50,
    backgroundColor:'rgb(240,240,240)'
  },
  input: {
    width: 240,
    height: 30,
    marginRight: 15,
    borderBottomWidth: 1,
    backgroundColor: 'rgb(245,245,245)',
    padding: 5,
  },
  input_picker: {
    width: 240,
    height: 30,
    marginRight: 15,
    borderBottomWidth: 1,
    padding: 5,
  },
  btn_search: {
    width: 100,
    height: 10,
    margin: 12,
    backgroundColor: '#2596be',
    padding: 5
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
    color: '#2596be'
  },
  options_detail: {
    flexDirection:'row'
  },
  options_child: {
    width: 150,
    justifyContent: 'center',
    alignItems: 'center',
  }
});