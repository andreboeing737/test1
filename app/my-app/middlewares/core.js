const config = require('../config/config');
const axios = require('axios');
const {encryptHeaderKey,decrypt,encrypt} = require('./encryption');

var doGetData = function(params,callback){
    const headers = {
        authorization:getApiAuthorization()
    }
    var url = config.API_URL.BACKEND_GET_DATA;
    if (Object.keys(params).length){
        var query_url = new URLSearchParams(params);
        var query_string = query_url.toString();
        url += "?"+query_string;
    }
    axios.get(url, {headers:headers})
    .then(function (response) {
        callback(null,response.data);
    })
    .catch(function (error) {
        console.log(error);
        callback(error,null);
    })
};

var doGetBookmark = function(callback){
    const headers = {
        authorization:getApiAuthorization()
    }
    axios.get(config.API_URL.BACKEND_GET_BOOKMARK, {headers:headers})
    .then(function (response) {
        var data = decrypt(response.data.data);
        data = JSON.parse(data);
        callback(null,data);
    })
    .catch(function (error) {
        console.log(error);
        callback(error,null);
    })
};

var doUpdateBookmark = function(form_data,callback){
    form_data = JSON.stringify(form_data);
    form_data = encrypt(form_data);
    form_data = {
        data:form_data
    }
    const headers = {
        authorization:getApiAuthorization()
    }
    axios.post(config.API_URL.BACKEND_UPDATE_BOOKMARK, form_data, {headers:headers})
    .then(function (response) {
        var data = {"message":"Success"};
        callback(null,data);
    })
    .catch(function (error) {
        console.log(error);
        callback(error,null);
    })
};

function getApiAuthorization(){
    return encryptHeaderKey(config.HEADER_KEY);
}

exports.doGetData = doGetData;
exports.doGetBookmark = doGetBookmark;
exports.doUpdateBookmark = doUpdateBookmark;