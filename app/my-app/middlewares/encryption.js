'use strict';
const config = require('../config/config')
import CryptoES from 'crypto-es';

const ENC_KEY = config.ENCRYPTION_KEY;
const ENC_KEY_HEADER = config.ENCRYPTION_KEY_HEADER;

var encrypt = ((val) => {
  const encrypted = CryptoES.AES.encrypt(val, ENC_KEY).toString();;
  return encrypted;
});

var decrypt = ((encrypted) => {
  var decrypted = CryptoES.AES.decrypt(encrypted, ENC_KEY);
  decrypted = decrypted.toString(CryptoES.enc.Utf8);
  return decrypted;
});

var encryptHeaderKey = ((val) => {
  const encrypted = CryptoES.AES.encrypt(val, ENC_KEY_HEADER).toString();;
  return encrypted;
});

var decryptHeaderKey = ((encrypted) => {
  const decrypted = CryptoES.AES.decrypt(encrypted, ENC_KEY_HEADER).toString(CryptoES.enc.Utf8);;
  return decrypted;
});

exports.encrypt = encrypt;
exports.decrypt = decrypt;
exports.encryptHeaderKey = encryptHeaderKey;
exports.decryptHeaderKey = decryptHeaderKey;